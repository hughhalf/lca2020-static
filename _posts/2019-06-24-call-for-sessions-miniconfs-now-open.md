---
layout: post
title: Call for Sessions and Miniconfs now open!
card: call_session_miniconf.46af9797.png
---
The linux.conf.au 2020 organising team is excited to announce that the linux.conf.au 2020 Call for Sessions and Call for Miniconfs are now open!
These will stay open from Monday 24 June to Sunday 28 July [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth).

Our theme for linux.conf.au 2020 is "Who's Watching", focusing on security, privacy and ethics.
As big data and IoT-connected devices become more pervasive, it's no surprise that we're more concerned about privacy and security than ever before.
We've set our sights on how open source could play a role in maximising security and protecting our privacy in times of uncertainty.
With the concept of privacy continuing to blur, open source could be the solution to give us '2020 vision'.

## Call for Sessions
Would you like to talk in the main conference of linux.conf.au 2020?
The main conference runs from Wednesday to Friday, with multiple streams catering for a wide range of interest areas.
We welcome you to submit a [session](/programme/sessions/) proposal for either a talk or tutorial now.

## Call for Miniconfs
Miniconfs are dedicated day-long streams focusing on single topics, creating a more immersive experience for delegates than a session.
Miniconfs are run on the first two days of the conference before the main conference commences on Wednesday.
If you would like to organise a [miniconf](/programme/miniconfs/) at linux.conf.au, we want to hear from you.

## Have we got you interested?
You can find out how to submit your session or miniconf proposals [here](/programme/proposals/).
If you have any other questions you can contact us via [email](mailto:contact@lca2020.linux.org.au).

We are looking forward to reading your submissions.